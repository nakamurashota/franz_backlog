'use strict';

module.exports = Franz => {
    const path = require('path');

    // inject a single css file
    Franz.injectCSS(path.join(__dirname, 'style.css'));
};

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNoYXR3b3JrL2luZGV4LmpzIl0sIm5hbWVzIjpbIm1vZHVsZSIsImV4cG9ydHMiLCJGcmFueiJdLCJtYXBwaW5ncyI6Ijs7QUFBQTtBQUNBQSxPQUFPQyxPQUFQLEdBQWlCQyxTQUFTQSxLQUExQiIsImZpbGUiOiJjaGF0d29yay9pbmRleC5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIGp1c3QgcGFzcyB0aHJvdWdoIEZyYW56XG5tb2R1bGUuZXhwb3J0cyA9IEZyYW56ID0+IEZyYW56O1xuIl19